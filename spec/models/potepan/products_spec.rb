require 'rails_helper'
RSpec.describe Potepan::ProductDecorator, type: :model do
  describe 'related_products' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:related_product) { create(:product, taxons: [taxon]) }
    let!(:unrelated_product) { create(:product) }

    subject do
      product.related_products
    end

    it '関連商品を取得できる' do
      is_expected.to include related_product
    end

    it '関連商品の中に同じtaxonに属していない商品が含まれない' do
      is_expected.to_not include unrelated_product
    end

    it '関連商品の中に自身である商品が含まれない' do
      is_expected.to_not include product
    end
  end
end
