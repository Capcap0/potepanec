require 'rails_helper'

RSpec.describe "Potepan::CategoriesController", type: :request do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'リクエストが成功する' do
      expect(response).to have_http_status(200)
    end

    it 'taxonomyが取得できる' do
      expect(response.body).to include taxonomy.name
    end

    it 'taxonが取得できる' do
      expect(response.body).to include taxon.name
    end

    it 'taxonに紐付けられたproductsが取得できる' do
      taxon.products.each do |product|
        expect(response.body).to include product.images.url(:large)
        expect(response.body).to include product.name
        expect(response.body).to include product.display_price
      end
    end
  end
end
