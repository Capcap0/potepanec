require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:max_related_products) { 4 }
    let!(:related_products) { create_list(:product, max_related_products + 1, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it 'リクエストが成功する' do
      expect(response).to have_http_status(200)
    end

    it '対象の商品情報が取得できる' do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
      product.images.each do |image|
        expect(response.body).to include image.url(:large)
        expect(response.body).to include image.url(:small)
      end
    end

    it '関連商品を4つ取得できる' do
      expect(controller.instance_variable_get(:@related_products).size).to eq max_related_products
    end
  end
end
