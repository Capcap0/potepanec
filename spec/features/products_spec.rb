require 'rails_helper'
RSpec.feature 'Products', type: :feature do
  around do |example|
    original = Capybara.ignore_hidden_elements
    Capybara.ignore_hidden_elements = false
    example.run
    Capybara.ignore_hidden_elements = original
  end

  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
  let!(:unrelated_taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:no_taxon_product) { create(:product) }
  let!(:related_product) { create(:product, name: 'related product', taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, taxons: [unrelated_taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario '該当ページが正しく表示される' do
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
    within '.productBox' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).to_not have_content unrelated_product.name
    end
  end

  scenario 'ヘッダーのブランド画像からトップページへ移動' do
    click_on 'BIGBAG_logo'
    expect(current_path).to eq potepan_path
  end

  scenario 'ヘッダーのHOMEボタンからトップページへ移動' do
    click_on 'HOME'
    expect(current_path).to eq potepan_path
  end

  scenario '「一覧ページへ戻る」リンクから商品が属するカテゴリー一覧へ移動' do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end

  scenario '対象の商品がtaxonに紐付いていない場合、「一覧ページ戻る」リンクが表示されない' do
    visit potepan_product_path(no_taxon_product.id)
    within '.media-body' do
      expect(page).to_not have_content '一覧ページへ戻る'
    end
  end

  scenario '関連商品として表示された商品をクリックしたとき、その商品詳細ページへ移動' do
    within '.productBox' do
      click_on related_product.name
    end
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
