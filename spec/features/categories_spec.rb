require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon1) { create(:taxon, name: 'Bags', parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxon2) { create(:taxon, name: 'Mugs', parent: taxonomy.root, taxonomy: taxonomy) }
  let!(:product1) { create(:product, name: 'Ruby-Bag', price: '10.00', taxons: [taxon1]) }
  let!(:product2) { create(:product, name: 'Ruby-Mug', price: '5.00', taxons: [taxon2]) }

  before do
    visit potepan_category_path(taxon1.id)
  end

  scenario 'カテゴリーページが正しく表示される' do
    expect(page).to have_title "#{taxon1.name} - BIGBAG Store"
    within '.navbar-side-collapse' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon1.name
      expect(page).to have_content taxon2.name
    end
    within '.productBox' do
      expect(page).to have_content product1.name
      expect(page).to have_content product1.price
      expect(page).to_not have_content product2.name
      expect(page).to_not have_content product2.price
    end
  end

  scenario 'カテゴリーをクリックしたとき、選択したカテゴリーの商品一覧が表示される' do
    within '.navbar-side-collapse' do
      click_on taxon2.name
      expect(current_path).to eq potepan_category_path(taxon2.id)
    end
  end

  scenario '商品をクリックしたとき、商品詳細ページが表示される' do
    within '.productBox' do
      click_on product1.name
    end
    expect(current_path).to eq potepan_product_path(product1.id)
  end
end
