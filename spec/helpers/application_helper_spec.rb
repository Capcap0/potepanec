require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_title_method' do
    context '文字列の引数を渡す場合' do
      let(:page_title) { 'BAG' }
      it { expect(full_title(page_title)).to eq 'BAG - BIGBAG Store' }
    end

    context '引数が空文字の場合' do
      let(:page_title) { '' }
      it { expect(full_title(page_title)).to eq 'BIGBAG Store' }
    end

    context '引数が空白の場合' do
      let(:page_title) { ' ' }
      it { expect(full_title(page_title)).to eq 'BIGBAG Store' }
    end

    context '引数がnilの場合' do
      let(:page_title) { nil }
      it { expect(full_title(page_title)).to eq 'BIGBAG Store' }
    end

    context '引数がない場合' do
      it { expect(full_title).to eq 'BIGBAG Store' }
    end
  end
end
